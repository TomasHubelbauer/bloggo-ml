# Machine Learning

- [Interesting thing](https://news.ycombinator.com/item?id=15919115)
- [Tensorflow WSL](https://gist.github.com/wall72/4bbe01b9df780c0c20a3e346ae6543e0)

## Idea

Try to see if I can train a network to recognize approximate position in a song
from a continuous audio stream. Could be useful for live shows where the AP
would feed into the network and it would give its best guess about where in the
setlist we're at so the projector could display corresponding visuals in the
background.

Related audio links:

- http://slazebni.cs.illinois.edu/spring17/lec26_audio.pdf
- https://github.com/ybayle/awesome-deep-learning-music
- https://towardsdatascience.com/audio-processing-in-tensorflow-208f1a4103aa
- https://www.tensorflow.org/versions/master/tutorials/audio_recognition
- http://cs231n.stanford.edu/reports/2016/pdfs/220_Report.pdf
- https://datascience.stackexchange.com/q/10025
- https://arxiv.org/pdf/1607.03681.pdf
- https://arxiv.org/pdf/1703.04770.pdf
- https://www.analyticsvidhya.com/blog/2017/11/heart-sound-segmentation-deep-learning/
